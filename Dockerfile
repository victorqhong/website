FROM ruby

RUN gem install jekyll RedCloth jekyll-textile-converter
COPY . /usr/local/src/website
WORKDIR /usr/local/src/website

CMD ["jekyll", "serve"]
