---
title: Hello World
layout: writing
excerpt: A new blog with my own domain....
---

<h2>{{ page.title }} <small>{{ page.date | date: "%B %e, %Y" }}</small></h2>

A new blog with my own domain. I didn't want to pay for hosting just for some static pages, so I'm using Github Pages. Traditionally, when I have done front-end web development, I would write directly in HTML, which never worked that well. So I decided to try out Jekyll and RedCloth, a templating engine and a markdown language in Ruby.

I liked the thought of writing in Markdown but my experience with Jekyll and RedCloth hasn't been the best. I think Markdown is great, but Markdown with a templating engine is less great. The mental model of Jekyll seems very ad-hoc; it doesn't seem to be very modular and it's not clear what data or how data flows through the engine. RedCloth didn't do that much for me either. I still end up thinking in terms of HTML and presentation instead of content. What ever happend to MVC?
 
As the title suggests, this is the start of some web development ideas I've been wanting to do, so there's more to come.

__Hello World__
